# Running out of storage in an LXD Container? Easy fix with Hetzner Volume & LXD Storage Pool

We are a small manufacturer for garments and textile products located in Shanghai, China. For managing our business processes we are using the FOSS ERPNext since 2019.

located inside a, ubuntu 20.04 Server on Hetzner cloud we run LXD to achive some sort of isolation and agility for operating ERPNext. We treat the container rather like a VM, not for isolating microservices like you would do with a Docker- or kubernetes-based approach. This has proven to be really reliable, yet flexible workhorse for us and our situation.

## Situation

Let's say there is an ERPNext instance (or any other application) running on a Hetzner (or any other cloud provider) cloud server inside an LXD container.

Now it seems you grow out of storage. It happens and I am sure it's not only me, right? First ... pulse getting high, adrenaline flowing becasue the application is down (and you didn't plan for this) :-/ .

## Solution

Making use of Volumes for Hetzner Cloud Servers & LXD storage pool makes this very easy. I am sure similar approach can be applied on any given cloud provider (Linode, Digital Ocean, you name it ...).

Here is how you do it!

### 0.   stop & create snapshot of source container

`$~: lxc stop [ctn_source]`

`$~: lxc snapshot [ctn_source] [snapshot_name]`

### 1.   add a new Volume (in the admin panel of your provider) to your VM without mounting it

### 2.   add a new storage pool to your LXD

`$~: lxc storage create [pool_name] zfs source=/dev/sdX`

### 3.   see the new storage pool

`$~: lxc storage list`

### 4.   copy tho old container to the new storage pool

`$~: lxc copy [ctn_source] [ctn_target] -s [pool_name]`

I believe you can do this with the entire container including all snapshots (like in the example here) as well as addressing a certain snapshot without moving all existing snapshots to the new container and the new storage pool it lives on.

### 5.   add proxy devices (port 80 / port 443) to the new container

`$~: lxc config device add [ctn_target] port80 proxy listen=tcp:123.45.67.89:80 connect=tcp:127.0.0.1:80`

`$~: lxc config device add [ctn_target] port43 proxy listen=tcp:123.45.67.89:443 connect=tcp:127.0.0.1:443`

(`123.45.67.89` being the public facing ip of the VM, the LXD host)

This routes requests hitting your VM's ip, or your domain respectively, from the outside world to the container.

### 6. start the new application instance

`$~: lxc start [ctn_target]`

### 7. check the new application instance

1.) `$~: lxc exec [ctn_target] -- systemctl status mysql`

I check the database instance powering ERPNext here. Any other check suitable for your application may do just as well.

2.) https://erp.yourdomain.net

### 8. test the new instance

### 9. delete the deprecated container, remove the old storage pool

- `$~: lxc delete [ctn_source]`

- `$~: lxc storage delete [pool_name]`

### 10. delete the legacy Hetzner Volume

## Conclusion!

this is not a very sophisticated DevOpsi sort of setup, but has proven to be very practical and quite robust for an aplication with just a few users, not requireing a ton of tools playing together and likewise operable for someone without all too much experience in the container or DevOps realm (me at the time of setting this up).